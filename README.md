<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200" alt="Laravel Logo"></a></p>

```shell
cp .env.example .env
docker compose up -d
docker compose exec app composer i
docker compose restart app
```

### Створити api для реалізації сервісу по скороченню URL адрес (приклад [bitly.com](https://bitly.com)).

- Після передачі даних клієнта та обробки запиту, сервер має повернути відповідь в форматі {"shortLink": "http://localhost:888/forward/hKfC"}.
- При переході за скороченим посиланням користувача має перенаправити на кінцеву сторінку.
- Також необхідно збирати статистику кількості переходів в розрізі доменів, при цьому потрібно використати events.
- Вивести дані статистики по api. Відповідь має містити інформацію про кількість переходів та бути згрупованою по доменах з сортуванням за кількістю переходів. Приклад: [{"domain": "Google ", "redirects": 55},...].
- Доступ до статистики має бути закритий для публічного використання.
- Необхідно врахувати, що має бути можливість створювати скорочені посилання через консольні команди Laravel (саму реалізацію робити не потрібно).
- Покрити код тестами за допомогою phpunit. Показати code coverage.
